
package Controlador;

import Modelo.Usuario;
import view.Atendimento;

public class AtendimentoControlador{
    private final Atendimento view;
    
    public AtendimentoControlador(Atendimento view){
        this.view = view;
        
    }
 
    public void Acesso(){
        String cliente= view.getText_cliente().getText();
        String id = view.getText_id().getText();
        Object sabores= view.getSabores().getSelectedItem().toString();
        Object opcao = view.getOpcao().getSelectedItem().toString();
        Object combos = view.getCombos().getSelectedItem().toString();
        
        Usuario Modelo = new Usuario(id, cliente, opcao, sabores, combos);
            
    }

}