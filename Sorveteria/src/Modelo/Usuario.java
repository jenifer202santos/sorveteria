
package Modelo;

public class Usuario {
    private String id;
    private String cliente;
    private Object opcao;
    private Object sabores;
    private Object combos;
    
    

    public Usuario(String id, String cliente, Object opcao, Object sabores,Object combos ) {
        this.id = id;
        this.cliente = cliente;
        this.opcao = opcao;
        this.sabores = sabores;
        this.combos =  combos;
        
       
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Object getOpcao() {
        return opcao;
    }

    public void setOpcao(Object opcao) {
        this.opcao = opcao;
    }

    public Object getSabores() {
        return sabores;
    }

    public void setSabores(Object sabores) {
        this.sabores = sabores;
    }

    public Object getCombos() {
        return combos;
    }

    public void setCombos(Object combos) {
        this.combos = combos;
    }
    
    
}