package view;
import Controlador.LoginControlador;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame {
    private final LoginControlador Controlador;

    public Login() {
        initComponents();
        Controlador = new LoginControlador(this);
        
    }
    public boolean checkLogin(String Usuario, String Senha){
       return Usuario.equals("Atendente") && Senha.equals("123456");
   }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Login = new javax.swing.JLabel();
        Nome = new javax.swing.JLabel();
        TextUsuario = new javax.swing.JTextField();
        Senha = new javax.swing.JLabel();
        Entrar = new javax.swing.JButton();
        TextSenha = new javax.swing.JPasswordField();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Login.setText("LOGIN");
        getContentPane().add(Login, new org.netbeans.lib.awtextra.AbsoluteConstraints(177, 40, 40, 40));

        Nome.setText("NOME:");
        getContentPane().add(Nome, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, -1, -1));
        getContentPane().add(TextUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 110, 210, 30));

        Senha.setText("SENHA:");
        getContentPane().add(Senha, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        Entrar.setText("Entrar");
        Entrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EntrarActionPerformed(evt);
            }
        });
        getContentPane().add(Entrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(152, 258, 104, -1));
        getContentPane().add(TextSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 180, 220, 30));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void EntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EntrarActionPerformed
        if(checkLogin(TextUsuario.getText(), new String(TextSenha.getPassword()))){
            Menu Menu = new Menu();
            Menu.setVisible(true);
            this.dispose();
            
        }else{
            this.Controlador.mensagem();
            TextUsuario.setText("");
            TextSenha.setText("");
        }
    }//GEN-LAST:event_EntrarActionPerformed

   
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

      
        java.awt.EventQueue.invokeLater(() -> {
            new Login().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Entrar;
    private javax.swing.JLabel Login;
    private javax.swing.JLabel Nome;
    private javax.swing.JLabel Senha;
    private javax.swing.JPasswordField TextSenha;
    private javax.swing.JTextField TextUsuario;
    // End of variables declaration//GEN-END:variables

    
    public void exibeMensagem(String Mensagem) {
        JOptionPane.showMessageDialog(null, Mensagem);
    }
    
   
   
   
    }


