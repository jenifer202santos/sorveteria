
package view;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class Atendimento extends javax.swing.JFrame {


    public Atendimento() {
        initComponents();
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Atendimento = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        Text_id = new javax.swing.JTextField();
        cliente = new javax.swing.JLabel();
        Opcao = new javax.swing.JComboBox();
        opção = new javax.swing.JLabel();
        Tabelita = new javax.swing.JScrollPane();
        tbl_Atendimento = new javax.swing.JTable();
        Sorvete = new javax.swing.JLabel();
        Sabores = new javax.swing.JComboBox();
        Combos = new javax.swing.JComboBox();
        Açai = new javax.swing.JLabel();
        Text_cliente = new javax.swing.JTextField();
        btn_Salvar = new javax.swing.JButton();
        bnt_pagar = new javax.swing.JButton();
        bnt_Cancelar = new javax.swing.JButton();
        Menuzinho = new javax.swing.JMenuBar();
        Home = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        Atendimento.setText("ATENDIMENTO");

        id.setText("Id da mesa:");

        cliente.setText("Nome do Cliente:");

        Opcao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Açaí", "Sorvete", "Açaí e Sorvete" }));

        opção.setText("Opções do Menu:");

        tbl_Atendimento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id", "Cliente", "Opção", "Sorvete e Preço", "Açai e Preço"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        Tabelita.setViewportView(tbl_Atendimento);

        Sorvete.setText("Sabores Sorvete:");

        Sabores.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Sabor Lara - R$ 7.00", "Sabor Jenifer - R$ 7.00", "Sabor Keyliane - R$ 10.00", "Sabor Cleberson - R$ 6.00", "Sabor Pikachu - R$ 8.00" }));

        Combos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Combo da Lara - R$ 7.00", "Combo da Jenifer - R$ 10.00", "Combo da Keyliane - R$ 8.00", "Combo do Cleberson - R$ 13.00", "Combo do Pikachu - R$ 15.00" }));

        Açai.setText("Combos Açai:");

        btn_Salvar.setText("Salvar");
        btn_Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SalvarActionPerformed(evt);
            }
        });

        bnt_pagar.setText("Pagar");
        bnt_pagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnt_pagarActionPerformed(evt);
            }
        });

        bnt_Cancelar.setText("Cancelar");
        bnt_Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnt_CancelarActionPerformed(evt);
            }
        });

        Home.setText("Home");

        jMenuItem1.setText("Menu");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        Home.add(jMenuItem1);

        Menuzinho.add(Home);

        setJMenuBar(Menuzinho);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 464, Short.MAX_VALUE)
                .addComponent(btn_Salvar)
                .addGap(27, 27, 27)
                .addComponent(bnt_pagar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(bnt_Cancelar)
                .addGap(160, 160, 160))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opção)
                            .addComponent(Sorvete)
                            .addComponent(Açai)
                            .addComponent(cliente)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(id)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(Combos, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Opcao, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Text_id, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                            .addComponent(Sabores, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(Text_cliente))
                        .addGap(18, 18, 18)
                        .addComponent(Tabelita, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(293, 293, 293)
                        .addComponent(Atendimento)))
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Atendimento)
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(id))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Text_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Opcao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(opção))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Sorvete)
                            .addComponent(Sabores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Combos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Açai)))
                    .addComponent(Tabelita, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bnt_pagar)
                    .addComponent(btn_Salvar)
                    .addComponent(bnt_Cancelar))
                .addGap(96, 96, 96))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SalvarActionPerformed
     DefaultTableModel dtmTable = (DefaultTableModel) tbl_Atendimento.getModel();
     Object[] dados = {Text_id.getText(), Text_cliente.getText(),Opcao.getSelectedItem(),Sabores.getSelectedItem(), Combos.getSelectedItem()};
     dtmTable.addRow(dados);
     Text_cliente.setText("");
     Text_id.setText("");
     Sabores.setSelectedItem(null);
     Opcao.setSelectedItem(null);
     Combos.setSelectedItem(null);
    }//GEN-LAST:event_btn_SalvarActionPerformed

    private void bnt_pagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnt_pagarActionPerformed
        if(tbl_Atendimento.getSelectedRow() != -1){
            DefaultTableModel dmtTable = (DefaultTableModel) tbl_Atendimento.getModel();
            dmtTable.removeRow(tbl_Atendimento.getSelectedRow());
        }
    }//GEN-LAST:event_bnt_pagarActionPerformed

    private void bnt_CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnt_CancelarActionPerformed
        if(tbl_Atendimento.getSelectedRow() != -1){
            DefaultTableModel dmtTable = (DefaultTableModel) tbl_Atendimento.getModel();
            dmtTable.removeRow(tbl_Atendimento.getSelectedRow());
        }
    }//GEN-LAST:event_bnt_CancelarActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Menu menu = new Menu();
        menu.setVisible(true);
        

    }//GEN-LAST:event_jMenuItem1ActionPerformed

  
    public static void main(String args[]) {
       
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new Atendimento().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Atendimento;
    private javax.swing.JLabel Açai;
    private javax.swing.JComboBox Combos;
    private javax.swing.JMenu Home;
    private javax.swing.JMenuBar Menuzinho;
    private javax.swing.JComboBox Opcao;
    private javax.swing.JComboBox Sabores;
    private javax.swing.JLabel Sorvete;
    private javax.swing.JScrollPane Tabelita;
    private javax.swing.JTextField Text_cliente;
    private javax.swing.JTextField Text_id;
    private javax.swing.JButton bnt_Cancelar;
    private javax.swing.JButton bnt_pagar;
    private javax.swing.JButton btn_Salvar;
    private javax.swing.JLabel cliente;
    private javax.swing.JLabel id;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLabel opção;
    private javax.swing.JTable tbl_Atendimento;
    // End of variables declaration//GEN-END:variables

    public JComboBox getCombos() {
        return Combos;
    }

    public void setCombos(JComboBox Combos) {
        this.Combos = Combos;
    }

    public JComboBox getOpcao() {
        return Opcao;
    }

    public void setOpcao(JComboBox Opcao) {
        this.Opcao = Opcao;
    }

    public JComboBox getSabores() {
        return Sabores;
    }

    public void setSabores(JComboBox Sabores) {
        this.Sabores = Sabores;
    }

    public JTextField getText_cliente() {
        return Text_cliente;
    }

    public void setText_cliente(JTextField Text_cliente) {
        this.Text_cliente = Text_cliente;
    }

    public JTextField getText_id() {
        return Text_id;
    }

    public void setText_id(JTextField Text_id) {
        this.Text_id = Text_id;
    }

}